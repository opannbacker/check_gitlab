# check_gitlab

<a href="https://coindrop.to/marcopeterseil" target="_blank"><img src="https://coindrop.to/embed-button.png" style="border-radius: 10px; height: 57px !important;width: 229px !important;" alt="Coindrop.to me"></img></a> Support goes 100% to [local animal hospice](https://www.animal-spirit.at/)

Gitlab Naemon/Icinga/Nagios plugin which checks various stuff via Gitlab API(v4) and output of gitlab-ctl services.

[[_TOC_]]

## Requirements
* Ruby >2.3

## Options
* -s, -H: gitlab url, only https supported, https://gitlab.example.com

* -k: if you've a self signed cert

* -t: access token, required for all api calls except health mode

* -i: project id, ci-runner id

* -n: name of group (regex), sidekiq jobs

* -e: exclude (regex)

* -d: debug/verbose output. at the moment only possible with _health_ mode

## Modes
### Health check
Checks the status of the readiness endpoint.
Check [here](https://docs.gitlab.com/ce/user/admin_area/monitoring/health_check.html) for more information.

Optional debug/verbose output possible via `-d`.

```shell
naemon@gitlab:plugins$ ./check_gitlab.rb -m health -s <gitlab_url> [-k]
OK - Gitlab probes are in healthy state
```

### Services
If any service of 'gitlab-ctl status' reports down service status will be critical.

Requires a sudo entry to get output of gitlab-ctl command.

```conf
naemon ALL = NOPASSWD: /usr/bin/gitlab-ctl status
```

```shell
naemon@gitlab:plugins$ ./check_gitlab.rb -m services
Critical - logrotate, mattermost is down
```

### CI Pipeline duration
Checks duration of finished CI pipeline in seconds with perfdata. ([/projects/:id/pipelines](https://docs.gitlab.com/ee/api/pipelines.html#list-project-pipelines))

```shell
naemon@gitlab:plugins$ ./check_gitlab.rb -m ci-pipeline-duration -s <gitlab_url> [-k] -t <access_token> -i <procject_id> -w 100 -c 200
Critical - Pipeline #265 took 265s | duration=265s;100;150
```

### CI Pipeline status
Checks status of latest CI pipeline. ([/projects/:id/pipelines](https://docs.gitlab.com/ee/api/pipelines.html#list-project-pipelines))

Options `-w` and `-c` allows regex.

Possible values for a ci pipeline status can be found [here](https://docs.gitlab.com/ee/api/pipelines.html#list-project-pipelines).

```shell
naemon@gitlab:plugins$ ./check_gitlab.rb -m ci-pipeline-status -s <gitlab_url> [-k] -t <access_token> -i <procject_id> -w pending -c 'failed|skipped'
OK - Status of pipeline #266693109: Success
```

### CI Runner status
Checks status of all registered CI runners. ([/runners/all](https://docs.gitlab.com/ee/api/runners.html#list-all-runners))

Access token needs admin privileges.

Possible `--status` values are listed [here](https://docs.gitlab.com/ee/api/runners.html#list-all-runners)

Exclude runners via `--exclude` (regex)

```shell
naemon@gitlab:plugins$ ./check_gitlab.rb -m ci-runner-status -s <gitlab_url> [-k] -t <access_token> -w 2 -c 5 --status offline
Critical - Offline runners: srv-cir-04(10), workbot15(63), srv-cir-01(76), srv-cir-05(111), srv-cir-07(117), srv-cir-08(121), mac-01(131)
```

### CI Runner jobs duration
Checks duration (in seconds) of the first found running job with perfdata. ([/runners/:id/jobs](https://docs.gitlab.com/ee/api/runners.html#list-runner-s-jobs))

```shell
naemon@gitlab:plugins$ ./check_gitlab.rb -m ci-runner-jobs-duration -s <gitlab_url> [-k] -t <access_token> -i <ci_runner_id> -w 5 -c 10
Critical - ansible_lint is running for 15s | duration=15s;5;10
```

### Group size
Checks size of all groups in megabytes with perfdata. ([/groups](https://docs.gitlab.com/ee/api/groups.html#list-groups))

Access token needs admin privileges.

Specify `--name` or `--exclude` (both use regex) option to limit the output (max 100 groups).

```shell
naemon@gitlab:plugins$ ./check_gitlab.rb -m group_size -s <gitlab_url> [-k] -t <access_token> -w 4072 -c 5096 -n '(DevOps|Docs)'
OK - DevOps: used space 191MB, Docs: used space 17MB | DevOps=191MB;4072;5096 Docs=17MB;4072;5096
```

### License
For license checks access token needs admin privileges.

#### Expire
Check remaining days when license expires. Specify less then remaining days via warning threshold. ([/license](https://docs.gitlab.com/ee/api/license.html))

```shell
naemon@gitlab:plugins$ ./check_gitlab.rb -m license-expire -s <gitlab_url> [-k] -t <access_token> -w 40
Warning - License  will expire at 2019-08-22 - 51 days left
```

#### Overage
Check for over-subscription. ([/license](https://docs.gitlab.com/ee/api/license.html))

```shell
naemon@gitlab:plugins$ ./check_gitlab.rb -m license-overage -s <gitlab_url> [-k] -t <access_token> -w 1 -c 5
OK - Active users: 43, Overage: 0 | active_users=43 overage=0;1;5
```

### Sidekiq
For Sidekiq checks access token needs admin privileges.

#### Job statistics
Check usage of performed Sidekiq jobs with perfdata. ([/sidekiq/job_stats](https://docs.gitlab.com/ee/api/sidekiq_metrics.html#get-the-current-job-statistics))

Use `--name` option to specify job. Possible values are listed [here](https://docs.gitlab.com/ee/api/sidekiq_metrics.html#get-the-current-job-statistics).

```shell
naemon@gitlab:plugins$ ./check_gitlab.rb -m sidekiq-jobs -s <gitlab_url> [-k] -t <access_token> -n dead -w 1 -c 5
OK - Sidekiq jobs - Dead: 0 | dead=0;2;5
```
