# changes by release

## 0.6.0
### features
* new check mode "ci-pipeline-status"

### other
* rename "ci-pipeline" to "ci-pipeline-duration"
* validate user input against possible modes

## 0.5.4
### other
* remove the optparser missing argument block

## 0.5.3
### fixes
* services: fix regex for names (#13)

## 0.5.2
### fixes
* ci-runner-jobs-duration: newer gitlab versions don't send content-length response header back when there is no running job.

## 0.5.1
### fixes
* ci-runner-status: fix wrong output when json response is empty

## 0.5.0
### features
* new sidekiq-jobs mode: check usage of sidekiq job metrics.
* ci-runner-status mode (former ci-runner) - you can now specify the desired state via `--status`. Checks all registered runners.
Outputs the runner names. Supports `--exlude` with regex.
* group-size - now fetches all groups (till 100) instead of one. Use `--name` option to select or `--exclude`.

### other
* added `-H` option to specify server/host address as an alternative to `-s`
* changed mode name `group_size` to `group-size`

### fixes
* health mode: fix new json data - now supports gitlab >12.4 (!9)

## 0.4.0
### features
* new license checks - license-expire and license-overage

### other
* license file added
* rubocop - rescue exception var name

## 0.3.2
### fixes
* ci-pipeline: added *finished* scope in api call to avoid duration "null" exception (#7)
* ci-runner: added perf data
* group_size: remove obsolete method call

## 0.3.1
### fixes
* check for correct sudoers entry (#5)

## 0.3
### features
* new mode - ci runner jobs duration: checks duration of first found running job on a ci runner

### other
* health mode - access token has been removed due to deprecation. use [IP whitelists](https://docs.gitlab.com/ee/user/admin_area/monitoring/health_check.html#ip-whitelist) instead.

## 0.2
### features
* new mode - group size: checks size of group/subgroup
* debug/verbose output for health check (!3)

### fixes
* catch all non 200 code responses as unknown status instead of a few selected (!4)

### other
* add helper methods and refactor some code
* empty search response produces an unknown status

## 0.1.1
* using v4 api endpoint
* health mode: using readiness probes endpoint instead of deprecated health_check - https://docs.gitlab.com/ce/user/admin_area/monitoring/health_check.html#using-the-endpoint

### fixes
* pipeline mode: only reports with status 'success'. running and skipped pipelines will not generate an error anymore.

## 0.1
* Initial release
